package com.test.phonebook;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;

import android.content.Context;
import android.util.Log;

public class PhonebookCollection extends ArrayList<PhoneItem> {

	public static PhonebookCollection createPhonebookCollection(Context context) {

		PhonebookCollection items;

		Gson gson = new Gson();
		items = gson.fromJson(readFromFile(context),
				new TypeToken<PhonebookCollection>() {
				}.getType());
		if (items == null)
			items = new PhonebookCollection();

		return items;

	}

	private PhonebookCollection() {
	}

	public void save(Context context) {

		String json = serialize();

		writeToFile(json, context);

	}

	public String serialize() {
		Gson gson = new Gson();
		String json = gson.toJson(this);

		Log.i(PhonebookCollection.class.getCanonicalName(),
				json);
		return json;
	}

	private static void writeToFile(String data, Context context) {
		try {
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
					context.openFileOutput("data.json", Context.MODE_PRIVATE));
			outputStreamWriter.write(data);
			outputStreamWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private static String readFromFile(Context context) {

		String ret = "";

		try {
			InputStream inputStream = context.openFileInput("data.json");

			if (inputStream != null) {
				InputStreamReader inputStreamReader = new InputStreamReader(
						inputStream);
				BufferedReader bufferedReader = new BufferedReader(
						inputStreamReader);
				String receiveString = "";
				StringBuilder stringBuilder = new StringBuilder();

				while ((receiveString = bufferedReader.readLine()) != null) {
					stringBuilder.append(receiveString);
				}

				inputStream.close();
				ret = stringBuilder.toString();
			}
		} catch (FileNotFoundException e) {
			Log.w(PhonebookCollection.class.getCanonicalName(),
					"Saved file not found.");
			return "";
		} catch (IOException e) {
			e.printStackTrace();
		}

		return ret;
	}

}
