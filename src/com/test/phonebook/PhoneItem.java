package com.test.phonebook;

import com.androidquery.AQuery;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;
import android.widget.Adapter;
import android.widget.RatingBar;

public class PhoneItem {
	public String name, phone, email;

	public void edit(Context context, final PhonebookAdapter adapter) {
		if (context == null)
			return;

		final AlertDialog.Builder dialog = new AlertDialog.Builder(
				context);

		dialog.setIcon(android.R.drawable.btn_star_big_on);
		dialog.setTitle("�������������� ������.");

		final View view = View.inflate(context, R.layout.phonebook_edit_dialog,
				null);

		AQuery aq = new AQuery(view);
		aq.id(R.id.editTextFIO).text(name);
		aq.id(R.id.editTextPhone).text(phone);
		aq.id(R.id.editTextEMail).text(email);

		dialog.setView(view);
		dialog.setPositiveButton("������",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						AQuery aq = new AQuery(view);

						name = aq.id(R.id.editTextFIO).getText().toString();
						phone = aq.id(R.id.editTextPhone).getText().toString();
						email = aq.id(R.id.editTextEMail).getText().toString();

						adapter.notifyDataSetChanged();

						dialog.dismiss();
					}
				})

		.setNegativeButton("������", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int id) {
				dialog.cancel();
			}
		});

		dialog.create();
		dialog.show();

	}

}
