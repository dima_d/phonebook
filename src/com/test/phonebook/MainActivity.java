package com.test.phonebook;

import org.json.JSONException;
import org.json.JSONObject;

import com.androidquery.AQuery;

import android.app.Activity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends Activity {

	private PhonebookAdapter mAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		AQuery aq = new AQuery(this);
		mAdapter = new PhonebookAdapter(this);
		aq.id(R.id.listView1).adapter(mAdapter);
		
	
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	class SendRequestMessage extends SendRequest {
		
		@Override
		public void onExecute(String result) {
			
			if (TextUtils.isEmpty(result))
			{
				Toast.makeText(MainActivity.this, "������ ����.", Toast.LENGTH_LONG).show();
				return;
			}
			
			try {
				JSONObject jsonObject = new JSONObject(result);
				
				int status = jsonObject.getInt("status");
				String message = jsonObject.getString("message");
				
				Toast.makeText(MainActivity.this,message, Toast.LENGTH_LONG).show();
				
			} catch (JSONException e) {
				e.printStackTrace();
			}
			
		}
	};

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		int id = item.getItemId();
		if (id == R.id.action_add) {
			mAdapter.addItem(this);
			return true;
		}
		if (id == R.id.action_save) {
			mAdapter.save(this);
			
			SendRequestMessage request = new SendRequestMessage();
			request.execute(mAdapter.getJSON());
			
			
			Logger.AddLog("���������� �������� �����: {JSON ���������}  ", this);
			
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
}
