package com.test.phonebook;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.Socket;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;

public abstract class SendRequest extends AsyncTask<String, Void, String>{
	
	public static final String URL_ADDRESS = "192.168.0.1"; // TODO: Set actual host
	public static final int URL_PORT = 50;


	@Override
	protected String doInBackground(String... params) {

		String request = params[0];
		StringBuilder stringBuilder = new StringBuilder();
		Socket socket = null;
		BufferedWriter bufferedWriter = null;
		BufferedReader bufferedReader = null;

		try {
			socket = new Socket(URL_ADDRESS, URL_PORT);
			bufferedWriter = new BufferedWriter(new OutputStreamWriter(
					socket.getOutputStream()));
			bufferedWriter.write("POST");
			bufferedWriter.write(request);
			bufferedWriter.flush();

			bufferedReader = new BufferedReader(new InputStreamReader(
					socket.getInputStream()));
			String s;
			while ((s = bufferedReader.readLine()) != null) {
				stringBuilder.append(s);
			}
			bufferedWriter.close();
			bufferedReader.close();

			socket.close();

			return stringBuilder.toString();
		} catch (IOException e) {
			e.printStackTrace();
		}

		return null;
	}
	
	public abstract void onExecute(String result); 

	
	@Override
	protected void onPostExecute(String result) {
		onExecute(result);
	}
	

}
