package com.test.phonebook;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import android.content.Context;
import android.util.Log;

public class Logger {

	public static void AddLog(String data, Context context) {
		try {
			
			StringBuilder builder = new StringBuilder();
			new Date();
			
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd hh:mm:ss ", Locale.getDefault());
			
			builder.append(sdf.format(new Date()));
			builder.append(data);
			
			Log.v(Logger.class.getCanonicalName(), builder.toString());

			
			OutputStreamWriter outputStreamWriter = new OutputStreamWriter(
					context.openFileOutput("log.txt", Context.MODE_APPEND));
			outputStreamWriter.write(builder.toString());
			outputStreamWriter.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
}
