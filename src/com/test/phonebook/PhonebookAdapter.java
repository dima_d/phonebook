package com.test.phonebook;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.List;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.androidquery.AQuery;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

public class PhonebookAdapter extends BaseAdapter {


	private PhonebookCollection items;

	public PhonebookAdapter(Context context) {

		items = PhonebookCollection.createPhonebookCollection(context);
	
	}

	@Override
	public int getCount() {
		return items.size();
	}

	@Override
	public PhoneItem getItem(int position) {
		return items.get(position);
	}

	@Override
	public long getItemId(int position) {
		return position;
	}
	
	public void addItem(Context context)
	{
		PhoneItem item = new PhoneItem();
		items.add(item);
		
		item.edit(context, this);
		
		notifyDataSetChanged();
	}
	
	public String getJSON()
	{
		return items.serialize();
	}
	
	public void save(Context context)
	{
		items.save(context);
	}
	
	
	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		if (parent == null|| parent.getContext() == null)
			return null;
		
		View view;
		
		if (convertView == null)
			view = View.inflate(parent.getContext(), R.layout.phonebook_item, null);
		else
			view = convertView;
		
		final PhoneItem item = items.get(position);
		
		AQuery aq = new AQuery(view);
		aq.id(R.id.textNumber).text(String.valueOf(position));
		aq.id(R.id.textFIO).text(item.name);
		aq.id(R.id.textPhone).text(item.phone);
		aq.id(R.id.textEMail).text(item.email);
		aq.id(R.id.buttonDel).clicked(new OnClickListener() {

			@Override
			public void onClick(View v) {
				items.remove(position);
				notifyDataSetChanged();
			}
		});
		

		aq.id(R.id.buttonEdit).clicked(new OnClickListener() {

			@Override
			public void onClick(View v) {
				item.edit(v.getContext(), PhonebookAdapter.this);
	
			}
		});

		
		
		return view;
	}



}
